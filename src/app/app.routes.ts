import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { GranjaComponent } from './components/granja/granja.component';
import { GallinaComponent } from './components/gallina/gallina.component';

const routes: Routes = [
    { path: 'home', component: HomeComponent },
    { path: ':id/granja', component: GranjaComponent},
    { path: ':id/gallina', component: GallinaComponent},
    { path: '**', pathMatch: 'full', redirectTo: 'home' }
];

export const appRouting = RouterModule.forRoot(routes);
