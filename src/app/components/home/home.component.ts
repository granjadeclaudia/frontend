import { Component, OnInit } from '@angular/core';
import { GranjaServices } from "../servicios/GranjaServices";
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  creoGranja = {
    name: ''
  }
  listaDeGranjas: any;

  reloadPage(){
    this.granjaservice.getGranjasAPI()
    .subscribe(data => {
      console.log(data);
      console.log(data.list);
      this.listaDeGranjas=data.list;
    },
      error => {
        console.log("Fallo el call de la api");
        console.log(error);
      
    });
  }

  constructor(private granjaservice:GranjaServices, private router:Router) { 

    this.reloadPage();
    console.log(this.listaDeGranjas );




  }

  ngOnInit() {
  }

  accederALaGranja(id: number){
    this.granjaservice.setGranjaId(id);
    console.log("Accedo a la granja " + id);
    this.router.navigate(['/' + id + '/granja']);
  }

   //Mata a la gallina llamando la API DELETE.
   borrarGranja(id: number){
    if(confirm("Borramos la granjita?")){
      this.granjaservice.deleteGranjaAPI(id)
    .subscribe(data =>{
      console.log("Granja borrada");
      this.reloadPage();
      
      
    },
    error =>{
      console.log("Fallo en el call de la api");
      console.log(error);
    });
    }
  }

  guardar(forma:NgForm){
    console.log("Creo nueva gallina");
    this.granjaservice.postGranjaAPI(this.creoGranja)
    .subscribe(data =>{
      console.log(  data)
      this.reloadPage();
    },
    error => {
      console.log("Fallo el call de la api");
      console.log(error);
    });
  }
}
