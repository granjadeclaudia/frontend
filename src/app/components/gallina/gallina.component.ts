import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { GranjaServices } from '../servicios/GranjaServices';

@Component({
  selector: 'app-gallina',
  templateUrl: './gallina.component.html',
  styleUrls: ['./gallina.component.css']
})
export class GallinaComponent implements OnInit {

  gallinanombre:any
  huevos:any
  huevosSinPadre: any

  creoGallina = {
    id: '',
    name: ''
  }
  updateHuevo={
    estado : 0
  }

  recargarPag(){
    this.activatedRouted.params.subscribe(params => {
      console.log("Parametro recibido: "+params['id']);

      this.granjaservice.getHuevosAPI(params['id'])
      .subscribe(data => {
        console.log("Recibo los huevillos")
        
        this.huevos = data.list;
        this.granjaservice.setGallinaId(params['id'])
        this.gallinanombre = data.gallinanombre;
        console.log(this.huevos)
      },
         error => {
           console.log("Fallo el call de la API");
         
           console.log(error)
         });
    })
  }

  // empollarHuevo(id:number){
  // console.log("Empollar el huevo: "+id)
  // this.updateHuevo.estado = 1;
  //   this.granjaservice.updateStatusHuevoAPI(this.updateHuevo,id).subscribe(data => {
  //     console.log("Recibo los huevillos cambiados(Nacido)")
      
  //     //this.huevos = data.list;
  //     //this.gallinanombre = data.gallinanombre;
  //     console.log(this.huevos)
  //     this.recargarPag()
  //   },
  //      error => {
  //        console.log("Fallo el call de la API");
       
  //        console.log(error)
  //      });
       
  // }

  empollarHuevo(id:number){
    var aux = prompt("Como se va a llamar?", "Sin nombre")
    if(aux !=null){
      console.log("Empollar el huevo: "+id)
      this.creoGallina.name = aux;
      this.updateHuevo.estado = 1;
      this.granjaservice.updateStatusHuevoAPI(this.updateHuevo,id).subscribe(data => {
      console.log("Recibo los huevillos cambiados(Nacido)")
      
      //this.huevos = data.list;
      //this.gallinanombre = data.gallinanombre;
      console.log(this.huevos)
      },
      error => {
      console.log("Fallo el call de la API");
      
      console.log(error)
      });
      
      this.granjaservice.postGallinaAPI(this.creoGallina).subscribe(data =>{
        console.log("Cree la gallina nueva");
        this.recargarPag();
      })
    }
    
   }

  volverAtras(){
    this.router.navigate([this.granjaservice.idGranjaLog+'/granja']);
    console.log('/home/'+this.granjaservice.idGallina+'/gallina')
  }

  tortillarHuevo(id:number){
    console.log("Tortillar el huevo: "+id)
    this.updateHuevo.estado = 0;
    this.granjaservice.updateStatusHuevoAPI(this.updateHuevo,id).subscribe(data => {
      console.log("Recibo los huevillos cambiados (tortilla)")
      
      // this.huevos = data.list;
      // this.gallinanombre = data.gallinanombre;
      console.log(this.huevos)
      this.recargarPag()

    },
       error => {
         console.log("Fallo el call de la API");
       
         console.log(error)
       });
  }
  constructor(private router:Router, private granjaservice: GranjaServices, private activatedRouted: ActivatedRoute ) 
  {
     this.recargarPag() 
  }

  ngOnInit() {
  }

}
