import { Injectable } from '@angular/core';
import 'rxjs/Rx';
import { Http, Headers } from '@angular/http';

@Injectable()
export class GranjaServices {

  //ATRIBUTOS
  orden: any;
  idGranjaLog;
  idGallina: any;
  listaDeGranjas: any;
  listaDeGallinas: any;
  listaDeHuevos: any;
  

  //SETTERS
  setListaDeGallinas(gallinas: any){
    this.listaDeGallinas = gallinas;
  }

  setOrden(orden: any){
    this.orden = orden;
  }

  setGranjaId(id: number){
    this.idGranjaLog = id;
  }

  setGallinaId(id:number){
    this.idGallina = id;
  }

//Busca las gallinas. Usado por la Searchbar
  buscarGallinas(termino:string){
    console.log("toy en serv")
    console.log()
    termino = termino.toLowerCase();
    let gallinasArray = [];

    for(let gallina of this.listaDeGallinas){
      let nombre = gallina.name.toLowerCase();
      
      if(nombre.indexOf(termino)>=0){
        gallinasArray.push(gallina);

      }
    }
    console.log("Devuelvo las gallinas!");
    return gallinasArray;

  }
  // ----------------------------- COMIENZO API's.

  //OBTIENE LA LISTA DE GRANJAS DESDE BDD
  getGranjasAPI() {
    console.log("Usando getGranjasAPI");
    let header = new Headers({ 'Content-Type': 'application/json' });
    let URL = "http://localhost:8080/granja/listargranjas";
    return this.http.get(URL, { headers: header })
      .map(res => {
        console.log("Entro positivo getGranjasAPI");
        this.listaDeGranjas = res.json().results;
        return res.json();
      }, err => console.log("error: " + err.json()));
  }

  //OBTIENE  LA LISTA DE GALLINAS DE CADA GRANJA DESDE BDD
  getGallinasAPI(id_granja: number) {
    console.log("Usando getGallinasAPI(id)");
    let header = new Headers({ 'Content-Type': 'application/json' });
    let URL = "http://localhost:8080/granja/listagallinas/"+ id_granja ;
    return this.http.get(URL, { headers: header })
      .map(res => {
        console.log("Entro positivo getGallinasAPI(id)");
        this.listaDeGallinas = res.json().results;
        return res.json();
      }, err => console.log("error: " + err.json()));
  }

  //OBTIENE  LA LISTA DE HUEVOS DE CADA GALLINA DESDE BDD
  getHuevosAPI(id_gallina: number) {
    console.log("Usando getHuevosAPI(id)");
    let header = new Headers({ 'Content-Type': 'application/json' });
    let URL = "http://localhost:8080/granja/"+ id_gallina +"/listahuevos";
    return this.http.get(URL, { headers: header })
      .map(res => {
        console.log("Entro positivo getHuevosAPI(id)");
        this.listaDeHuevos = res.json().results;
        return res.json();
      }, err => console.log("error: " + err.json()));
  }

  //CREA UNA NUEVA GALLINA
  postGallinaAPI(gallina: any) {
    console.log("Llamo a postGallinaAPI(gallina)");
    let header = new Headers({ 'Content-Type': 'application/json' });
    let URL = "http://localhost:8080/granja/" + this.idGranjaLog +"/nuevagallina";
    let body = gallina;
    return this.http.post(URL, body, { headers: header })
      .map(res => {
        console.log("Entro positivo postGallinaAPI(gallina)");
        return res.json();
      }, err => console.log("error: " + err.json()));
  }

  //CREA NUEVA GRANJA
  postGranjaAPI(gallina: any) {
    console.log("Llamo a postGallinaAPI(gallina)");
    let header = new Headers({ 'Content-Type': 'application/json' });
    let URL = "http://localhost:8080/granja/nuevagranja";
    let body = gallina;
    return this.http.post(URL, body, { headers: header })
      .map(res => {
        console.log("Entro positivo postGallinaAPI(gallina)");
        return res.json();
      }, err => console.log("error: " + err.json()));
  }
  //CREA UN NUEVO HUEVO
  postHuevoAPI(huevo: any) {
    console.log("Llamo a postHuevoAPI(huevo)");
    let header = new Headers({ 'Content-Type': 'application/json' });
    let URL = "http://localhost:8080/granja/"+this.idGallina+"/nuevohuevo";
    return this.http.post(URL, { headers: header })
      .map(res => {
        console.log("Entro positivo postHuevoAPI(huevo)");
        return res.json();
      }, err => console.log("error: " + err.json()));
  }


  //BORRA UNA GALLINA
  //HACETE UN ARROCITO CON POLLO CON LA GALLINA
   deleteGallinaAPI(id : number) {
     console.log("Llamo a deleteGallinaAPI");

     let header = new Headers({'Content-Type': 'application/json'});
     let URL = "http://localhost:8080/granja/"+this.idGranjaLog+"/farm/"+id+"/kill";

     return this.http.delete(URL, { headers: header}) 
       .map(res => {
         console.log("Entro positivo deleteGallinaAPI()");
         return res.json();
       }, err => console.log ("error: " + err.json()));
   }

   //BORRA UNA GRANJA
   deleteGranjaAPI(id : number) {
    console.log("Llamo a deleteGallinaAPI");

    let header = new Headers({'Content-Type': 'application/json'});
    let URL = "http://localhost:8080/granja/"+id+"/deletefarm";

    return this.http.delete(URL, { headers: header})
      .map(res => {
        console.log("Entro positivo deleteGranjaAPI()");
        return res.json();
      }, err => console.log ("error: " + err.json()));
  }

   updateStatusHuevoAPI(data:any,eggId:number){
    console.log("Llamo a updateStatusHuevo")
    let header = new Headers({'Content-Type': 'application/json'});
    let URL = "http://localhost:8080/granja/updatehuevo/"+eggId;
    let body = data;

    return this.http.post(URL, body, { headers: header })
      .map(res => {
        console.log("Entro positivo updateStatusHuevoAPI(huevo)");
        return res.json();
      }, err => console.log("error: " + err.json()));
    

   }

   updateNombreGallina(edit, id){
     console.log("Llamo a updateNombreGallina");
     let header = new Headers({'Content-Type': 'application/json'});
     let URL = "http://localhost:8080/granja/updategallinanombre/"+id;
     let body = edit;

     return this.http.post(URL, body, { headers:header })
      .map(res => {
        console.log("Entro positivo updateNombreGallina(nombre)");
        return res.json();
      }, err => console.log("error: " + err.json()));
   }

   updateCantHuevos(edit, id){
    console.log("Llamo a updateNombreGallina");
    let header = new Headers({'Content-Type': 'application/json'});
    let URL = "http://localhost:8080/granja/updategallinahuevos/"+id;
    let body = edit;

    return this.http.post(URL, body, { headers:header })
     .map(res => {
       console.log("Entro positivo updateNombreGallina(nombre)");
       return res.json();
     }, err => console.log("error: " + err.json()));
  }

  //------------------------------- FIN API´s.

  constructor(private http: Http) {
  }

}
