import { Component, OnInit } from '@angular/core';
import { GranjaServices} from './../servicios/GranjaServices';
import { GranjaComponent } from '../granja/granja.component';

@Component({
  selector: 'app-searchbar',
  templateUrl: './searchbar.component.html',
  styleUrls: ['./searchbar.component.css']
})
export class SearchbarComponent implements OnInit {

  constructor(private granjaservice:GranjaServices, private util: GranjaComponent) { }

  ngOnInit() {
  }

  buscarGallina(termino: string){
    console.log("Entre a buscarGallinas")
    let gallinasEncontradas = this.granjaservice.buscarGallinas(termino);
    console.log("Searchbar"+gallinasEncontradas);
    this.util.setGallinas(gallinasEncontradas);


  }

}
