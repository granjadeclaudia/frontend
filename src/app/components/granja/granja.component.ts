import { Component, OnInit } from '@angular/core';
import { GranjaServices } from '../servicios/GranjaServices';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-granja',
  templateUrl: './granja.component.html',
  styleUrls: ['./granja.component.css']
})
export class GranjaComponent implements OnInit {

  creoGallina = {
    id: '',
    name: ''
  }

  editGal = {
    nombre: '',
    huevos: ''
  }


  orden: number;
  gallinas: any;
  gallinasBackup: any;
  granjanombre: any;  
  huevo: any;
  huevosSinPadre: any;
  variable: any;

  ordenar(num: number){
    this.orden = num;
    this.granjaservice.setOrden(num);
    console.log(this.orden)
    console.log(this.granjaservice.orden)
    switch(num){
      case 1:{
        this.recargarPag();
        break;
      }
      case 2:{ 
        this.gallinas.sort((a, b) => parseFloat(b.eggs) - parseFloat(a.eggs));
        console.log("2");
      break;
      }
      case 3:{
        this.gallinas.sort((a, b) => parseFloat(a.eggs) - parseFloat(b.eggs));
        break;
      }
    }
  }

   editarNombre(nuevoName, id){
     this.editGal.nombre = nuevoName;
     console.log(this.editGal.nombre)
     this.granjaservice.updateNombreGallina(this.editGal, id).subscribe(data =>{
       console.log("Recibo el nuevo nombre de la gallina")
  
      
       },
       error => {
       console.log("Fallo el call de la API");
      
       console.log(error)
       });
   }

   editarHuevos(cantHuevos, id){

     if(cantHuevos>=0){
      this.editGal.huevos = cantHuevos;
      console.log(this.editGal.huevos);
      this.granjaservice.updateCantHuevos(this.editGal, id).subscribe(data =>{
        console.log("Recibo la cantidad de huevos nuevos que va a tener la gallina")
        },
        error =>{
          console.log("Fallo el call de la API");
          console.log(error);
       
      });
     }else{
       alert("No es una gallina cuantica, no puede tener huevos negativos. (ni letras)");
     }

   }


  recargarPag(){
    console.log(this.orden)
    console.log(this.granjaservice.orden)

    this.activatedRouted.params.subscribe(params => {
      console.log("Parametro recibido: "+params['id']);

      this.granjaservice.getGallinasAPI(params['id'])
      .subscribe(data => {  

        console.log("Recibo las gallinas")
        console.log(data)
        this.gallinas = data.list;
        this.granjanombre = data.name;
        this.huevosSinPadre = data.huevoshuerfanos;
        this.granjaservice.setGranjaId(params['id'])
        this.granjaservice.setListaDeGallinas(data.list);
        console.log(this.orden) 
        this.ordenar(this.granjaservice.orden)
        // this.granjaservice.setOrden(this.orden);
        // this.ordenar(this.granjaservice.orden)
        // console.log(this.orden)
      },
         error => {
           console.log("Fallo el call de la API");
         
           console.log(error)
         });
    })
  }

  setGallinas(gallinas: any){
    this.gallinas = gallinas;
    console.log(gallinas);
  }

  constructor(private granjaservice: GranjaServices, private router:Router, private activatedRouted:ActivatedRoute) { 

    
    this.orden = this.granjaservice.orden;
    this.recargarPag();
    console.log(this.gallinasBackup)



    
  }

  updateHuevo= {
    estado: 0
  }


  tortillarHuevo(id:number){
 
    console.log("Tortillar el huevo: "+id)
    this.updateHuevo.estado = 0;
    this.granjaservice.updateStatusHuevoAPI(this.updateHuevo,id).subscribe(data => {
    console.log("Recibo los huevillos cambiados (tortilla)")
    
    // this.huevos = data.list;
    // this.gallinanombre = data.gallinanombre;
    console.log(this.huevosSinPadre)
    this.recargarPag()
    
    },
    error => {
    console.log("Fallo el call de la API");
    
    console.log(error)
    });
    
   }
   
   empollarHuevo(id:number){
    var aux = prompt("Como se va a llamar?", "Sin nombre")
    if(aux !=null){
      console.log("Empollar el huevo: "+id)
      this.creoGallina.name = aux;
      this.updateHuevo.estado = 1;
      this.granjaservice.updateStatusHuevoAPI(this.updateHuevo,id).subscribe(data => {
      console.log("Recibo los huevillos cambiados(Nacido)")
      
      //this.huevos = data.list;
      //this.gallinanombre = data.gallinanombre;
      console.log(this.huevosSinPadre)
      },
      error => {
      console.log("Fallo el call de la API");
      
      console.log(error)
      });
      
      this.granjaservice.postGallinaAPI(this.creoGallina).subscribe(data =>{
        console.log("Cree la gallina nueva");
        this.recargarPag();
      })
    }
    
   }


  accederALaGallina(id: number){
    this.granjaservice.setGallinaId(id);
    console.log("Accedo a la Gallina " + id);
    this.router.navigate(['/' + id + '/gallina']);
  }

  


  ngOnInit() {
  }

  guardar(forma:NgForm){
    console.log("Creo nueva gallina");
    this.granjaservice.postGallinaAPI(this.creoGallina)
    .subscribe(data =>{
      console.log(  data)
      this.recargarPag();
    },
    error => {
      console.log("Fallo el call de la api");
      console.log(error);
    });
  }

  //Mata a la gallina llamando la API DELETE.
  matarGallina(id: number){
    if(confirm("Planeas ensuciarte las manos??")){
      this.granjaservice.deleteGallinaAPI(id)
    .subscribe(data =>{
      console.log("Gallina borrada");
      this.recargarPag();
      
      
    },
    error =>{
      console.log("Fallo en el call de la api");
      console.log(error);
    });
    }
  }

  agregarHuevo(id:number){
    this.granjaservice.setGallinaId(id);
   console.log("Creo un nuevo huevo");
   this.granjaservice.postHuevoAPI(this.huevo)
    .subscribe(data =>{
     console.log(data)
     this.recargarPag();
   },
   error => {
     console.log("Fallo el call de la api");
     console.log(error);
   });
 

}
}