import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { appRouting } from './app.routes';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { GranjaComponent } from './components/granja/granja.component';
import { HttpModule } from '@angular/http';
import { GranjaServices } from './components/servicios/GranjaServices';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FormsModule } from '@angular/forms';
import { SearchbarComponent } from './components/searchbar/searchbar.component';
import { GallinaComponent } from './components/gallina/gallina.component';
import { FooterComponent } from './components/footer/footer.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    GranjaComponent,
    NavbarComponent,
    SearchbarComponent,
    GallinaComponent,
    FooterComponent,
  ],
  imports: [
    BrowserModule,
    appRouting,
    HttpModule,
    FormsModule
  ],
  providers: [GranjaServices],
  bootstrap: [AppComponent]
})
export class AppModule { }
